# Project goffline

One Paragraph of project description goes here

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

## Docker Commands

Local
```bash
# Copy .env.sample -> .env and modify, then
make docker compose up
```

Production
```bash
# Copy .env.sample -> .env and modify, then
make docker compose --profile prod up
```
