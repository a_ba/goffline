# Build stage
FROM golang:1.23-alpine AS build

# Install build dependencies
RUN apk add --no-cache git

# Set build arguments
ARG CGO_ENABLED=0
ARG GOOS=linux
ARG GOARCH=amd64

WORKDIR /app

# Copy only dependency files first
COPY go.mod go.sum ./
RUN --mount=type=cache,target=/go/pkg/mod \
    --mount=type=cache,target=/root/.cache/go-build \
    go mod download

# Copy source code
COPY . .

# Build with optimization flags
RUN --mount=type=cache,target=/go/pkg/mod \
    --mount=type=cache,target=/root/.cache/go-build \
    go build -ldflags="-w -s" -o main cmd/api/main.go

# Production stage
FROM alpine:3.20.1 AS prod
RUN apk add --no-cache ca-certificates

WORKDIR /app
COPY --from=build /app/main /app/main

EXPOSE ${PORT}
CMD ["./main"]
