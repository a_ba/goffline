package database

import (
	"github.com/muktihari/fit/profile/typedef"
	"gorm.io/gorm"
	"time"
)

type ActivityLocation struct {
	gorm.Model
	Sessions      []*ActivitySession `gorm:"foreignKey:LocationID"` // Add this
	Latitude      float64
	Longitude     float64
	SessionID     uint
	Suburb        string
	Postcode      string
	State         string
	StateCode     string
	StateDistrict string
	County        string
	Country       string
	CountryCode   string
	City          string
}

type ActivitySet struct {
	gorm.Model
	ActivitySessions []ActivitySession `gorm:"foreignKey:ActivitySetID;constraint:OnDelete:SET NULL" json:"activity_set"` // Relationship
}

type ActivitySession struct {
	gorm.Model
	Records              []*ActivityRecord `gorm:"foreignKey:SessionID;constraint:OnDelete:CASCADE" json:"records"` // Relationship
	LocationID           *uint             `gorm:"default:null"`
	Location             *ActivityLocation `json:"location"`
	ActivitySetID        uint              `gorm:"default:null" json:"activity-set-id"`
	UserID               uint              `gorm:"not null" json:"-"`
	Filename             string
	Timestamp            time.Time            `json:"timestamp"` // Units: s; Session end time.
	StartTime            time.Time            `json:"start_time"`
	SportProfileName     string               `json:"sport_profile_name"` // Sport name from associated sport mesg
	StartPositionLat     float64              //`json:"start_position_lat,omitempty"`  // Units: semicircles
	StartPositionLong    float64              //`json:"start_position_long,omitempty"` // Units: semicircles
	TotalElapsedTime     float64              `json:"total_elapsed_time"` // Scale: 1000; Units: s; Time (includes pauses)
	TotalTimerTime       float64              `json:"total_timer_time"`   // Scale: 1000; Units: s; Timer Time (excludes pauses)
	TotalDistance        float64              `json:"total_distance"`     // Scale: 100; Units: m
	TotalCycles          uint32               `json:"total_cycles"`       // Units: cycles
	MessageIndex         typedef.MessageIndex `json:"message_index"`      // Selected bit is set for the current session.
	TotalCalories        uint16               `json:"total_calories"`     // Units: kcal
	AvgSpeed             float64              `json:"avg_speed"`          // Scale: 1000; Units: m/s; total_distance / total_timer_time
	MaxSpeed             float64              `json:"max_speed"`          // Scale: 1000; Units: m/s
	TotalAscent          uint16               `json:"total_ascent"`       // Units: m
	TotalDescent         uint16               `json:"total_descent"`      // Units: m
	FirstLapIndex        uint16               `json:"first_lap_index"`
	NumLaps              uint16               `json:"num_laps"`
	Event                typedef.Event        `json:"event"`      // session
	EventType            typedef.EventType    `json:"event_type"` // stop
	Sport                typedef.Sport        `json:"sport"`
	SubSport             typedef.SubSport     `json:"sub_sport"`
	AvgHeartRate         uint8                `json:"avg_heart_rate"`         // Units: bpm; average heart rate (excludes pause time)
	MaxHeartRate         uint8                `json:"max_heart_rate"`         // Units: bpm
	AvgCadence           uint8                `json:"avg_cadence"`            // Units: rpm; total_cycles / total_timer_time if non_zero_avg_cadence otherwise total_cycles / total_elapsed_time
	MaxCadence           uint8                `json:"max_cadence"`            // Units: rpm
	AvgFractionalCadence float64              `json:"avg_fractional_cadence"` // Scale: 128; Units: rpm; fractional part of the avg_cadence
	MaxFractionalCadence float64              `json:"max_fractional_cadence"` // Scale: 128; Units: rpm; fractional part of the max_cadence
}

type ActivityRecord struct {
	gorm.Model
	SessionID         uint
	Timestamp         time.Time `json:"timestamp"`
	PositionLat       float64   `json:"position_lat"`
	PositionLong      float64   `json:"position_long"`
	Distance          float64   `json:"distance"`
	Altitude          float64   `json:"altitude"`
	Speed             float64   `json:"speed"`
	HeartRate         uint8     `json:"heart_rate"`
	Cadence           uint8     `json:"cadence"`
	FractionalCadence float64   `json:"fractional_cadence"`
	EnhancedSpeed     float64   `json:"enhanced_speed"`
	EnhancedAltitude  float64   `json:"enhanced_altitude"`
}

type ActivityDateCount struct {
	Date  string `json:"date"`
	Count int    `json:"count"`
}

type User struct {
	gorm.Model
	NextCloudConfig NextCloudConfig
	Email           string `gorm:"unique;not null"`
	Password        string `json:"password"`
}
type NextCloudConfig struct {
	gorm.Model
	UserID   uint   `gorm:"not null" json:"user_id"`
	Host     string `gorm:"not null" json:"host"`
	Token    string `gorm:"not null" json:"token"`
	Password string `gorm:"not null" json:"password"`
}
