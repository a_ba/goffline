package database

import (
	"fmt"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"log"
)

var (
	host     = "postgres"
	port     = "5432"
	username = "goffline"
	password = "password1234"
	schema   = "public"
	database = "goffline"

	/*
		TODO Check why os.Getenv fails
			host     = os.Getenv("DB_HOST")
			port     = os.Getenv("DB_PORT")
			username = os.Getenv("DB_USERNAME")
			password = os.Getenv("DB_PASSWORD")
			schema   = os.Getenv("DB_SCHEMA")
			database = os.Getenv("DB_DATABASE")
	*/
)

// InitializeDatabase sets up and returns a *gorm.DB instance
func InitializeDatabase() (*gorm.DB, error) {
	dsn := fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=disable&search_path=%s", username, password, host, port, database, schema)
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{TranslateError: true})
	if err != nil {
		return nil, err
	}

	// Auto migrate the schema
	if err := db.AutoMigrate(
		&ActivitySet{},
		&ActivityLocation{},
		&ActivitySession{},
		&ActivityRecord{},
		&User{},
		&NextCloudConfig{},
	); err != nil {
		log.Fatal(err)
	}

	var user User
	db.FirstOrCreate(&user, User{Email: "aba@aba.com", Password: "aba"})
	db.FirstOrCreate(&NextCloudConfig{}, NextCloudConfig{UserID: user.ID, Host: "https://cloud.sinnefo.de", Token: "6ASQzfC37NLxpyq", Password: "LG8C9QPgi5"})

	log.Printf("Database connected successfully")
	return db, nil
}
