package repository

import (
	"goffline/internal/database"
	"gorm.io/gorm"
)

type NextCloudRepository interface {
	Create(config *database.NextCloudConfig) (*database.NextCloudConfig, error)
	GetByID(id uint) (*database.NextCloudConfig, error)
	GetByUserID(userID uint) (*database.NextCloudConfig, error)
	Update(config *database.NextCloudConfig) error
	Delete(id uint) error
	List() ([]*database.NextCloudConfig, error)
}

type nextCloudRepository struct {
	db *gorm.DB
}

func (r *nextCloudRepository) GetByUserID(userID uint) (*database.NextCloudConfig, error) {
	var config database.NextCloudConfig
	if err := r.db.Where("user_id = ?", userID).First(&config).Error; err != nil {
		return nil, err
	}
	return &config, nil
}

func NewNextCloudRepository(db *gorm.DB) NextCloudRepository {
	return &nextCloudRepository{db: db}
}

func (r *nextCloudRepository) Create(config *database.NextCloudConfig) (*database.NextCloudConfig, error) {
	if err := r.db.Create(config).Error; err != nil {
		return nil, err
	}
	return config, nil
}

func (r *nextCloudRepository) GetByID(id uint) (*database.NextCloudConfig, error) {
	var config database.NextCloudConfig
	if err := r.db.First(&config, id).Error; err != nil {
		return nil, err
	}
	return &config, nil
}

func (r *nextCloudRepository) Update(config *database.NextCloudConfig) error {
	if err := r.db.Model(&config).Updates(config).Error; err != nil {
		return err
	}
	return nil
}

func (r *nextCloudRepository) Delete(id uint) error {
	return r.db.Delete(&database.NextCloudConfig{}, id).Error
}

func (r *nextCloudRepository) List() ([]*database.NextCloudConfig, error) {
	var configs []*database.NextCloudConfig
	if err := r.db.Find(&configs).Error; err != nil {
		return nil, err
	}
	return configs, nil
}
