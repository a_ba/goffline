package repository

import (
	"goffline/internal/database"
	"gorm.io/gorm"
)

// ==== ActivityLocationRepository ====
type ActivityLocationRepository interface {
	Create(location *database.ActivityLocation) (*database.ActivityLocation, error)
	GetByID(id uint) (*database.ActivityLocation, error)
	Update(location *database.ActivityLocation) error
	Delete(id uint) error
	List() ([]*database.ActivityLocation, error)
	FindLocationsInRadius(lat float64, long float64, radius float64) ([]*database.ActivityLocation, error)
}

type activityLocationRepository struct {
	db *gorm.DB
}

// Constructor for ActivityLocationRepository
func NewActivityLocationRepository(db *gorm.DB) ActivityLocationRepository {
	return &activityLocationRepository{db: db}
}

func (r *activityLocationRepository) Create(location *database.ActivityLocation) (*database.ActivityLocation, error) {
	if err := r.db.Create(location).Error; err != nil {
		return nil, err
	}
	return location, nil
}

func (r *activityLocationRepository) GetByID(id uint) (*database.ActivityLocation, error) {
	var location database.ActivityLocation
	if err := r.db.First(&location, id).Error; err != nil {
		return nil, err
	}
	return &location, nil
}

func (r *activityLocationRepository) FindLocationsInRadius(lat float64, long float64, radius float64) ([]*database.ActivityLocation, error) {
	var locations []*database.ActivityLocation

	// Get all locations within a square bounding box
	err := r.db.Where("latitude BETWEEN ? AND ? AND longitude BETWEEN ? AND ?",
		lat-radius,
		lat+radius,
		long-radius,
		long+radius).
		Find(&locations).Error

	if err != nil {
		return nil, err
	}

	if len(locations) == 0 {
		return nil, nil
	}

	return locations, nil
}

func (r *activityLocationRepository) Update(location *database.ActivityLocation) error {
	return r.db.Save(location).Error
}

func (r *activityLocationRepository) Delete(id uint) error {
	return r.db.Delete(&database.ActivityLocation{}, id).Error
}

func (r *activityLocationRepository) List() ([]*database.ActivityLocation, error) {
	var locations []*database.ActivityLocation
	if err := r.db.Find(&locations).Error; err != nil {
		return nil, err
	}
	return locations, nil
}

// ==== ActivitySessionRepository ====
type ActivitySessionRepository interface {
	Create(session *database.ActivitySession) (*database.ActivitySession, error)
	UserSessionsWithoutActivitySet(userID uint) ([]*database.ActivitySession, error)
	UserSessionsWithActivitySet(userID uint) ([]*database.ActivitySession, error)
	UserSessionsWithoutLocation(userID uint) ([]*database.ActivitySession, error)
	GetByID(id uint) (*database.ActivitySession, error)
	ListUserSessions(userID uint) ([]*database.ActivitySession, error)
	GetMatchedActivities(sessionID uint) ([]*database.ActivitySession, error)
	Update(session *database.ActivitySession) error
	Delete(id uint) error
}

type activitySessionRepository struct {
	db *gorm.DB
}

func (r *activitySessionRepository) GetMatchedActivities(sessionID uint) ([]*database.ActivitySession, error) {
	var session *database.ActivitySession
	var matchedSessions []*database.ActivitySession
	if err := r.db.First(&session, sessionID).Error; err != nil {
		return nil, err
	}

	if err := r.db.Where("activity_set_id  = ?", session.ActivitySetID).Preload("Location").Find(&matchedSessions).Error; err != nil {
		return nil, err
	}
	return matchedSessions, nil
}

func (r *activitySessionRepository) UserSessionsWithoutLocation(userID uint) ([]*database.ActivitySession, error) {
	var sessions []*database.ActivitySession
	if err := r.db.Where("NOT EXISTS (?)", r.db.Table("activity_locations").
		Where("activity_locations.session_id = activity_sessions.id")).
		Where("user_id = ?", userID).
		Find(&sessions).Error; err != nil {
		return nil, err
	}
	return sessions, nil
}

func (r *activitySessionRepository) UserSessionsWithoutActivitySet(userID uint) ([]*database.ActivitySession, error) {
	var activities []*database.ActivitySession
	if err := r.db.
		Where("start_position_lat != 0 AND start_position_long != 0").
		Where("activity_set_id IS NULL").
		Where("user_id = ?", userID).
		Find(&activities).Error; err != nil {
		return nil, err
	}
	return activities, nil
}

func (r *activitySessionRepository) UserSessionsWithActivitySet(userID uint) ([]*database.ActivitySession, error) {
	var activities []*database.ActivitySession
	if err := r.db.
		Where("start_position_lat != 0 AND start_position_long != 0").
		Where("activity_set_id IS NOT NULL").
		Where("user_id = ?", userID).
		Find(&activities).Error; err != nil {
		return nil, err
	}
	return activities, nil
}

func NewActivitySessionRepository(db *gorm.DB) ActivitySessionRepository {
	return &activitySessionRepository{db: db}
}

func (r *activitySessionRepository) Create(session *database.ActivitySession) (*database.ActivitySession, error) {
	if err := r.db.Create(session).Error; err != nil {
		return nil, err
	}
	return session, nil
}

func (r *activitySessionRepository) GetByID(id uint) (*database.ActivitySession, error) {
	var session *database.ActivitySession
	if err := r.db.Where("id = ?", id).Preload("Location").Find(&session).Limit(1).Error; err != nil {
		return nil, err
	}
	return session, nil
}

func (r *activitySessionRepository) Update(session *database.ActivitySession) error {
	return r.db.Save(session).Error
}

func (r *activitySessionRepository) Delete(id uint) error {
	return r.db.Delete(&database.ActivitySession{}, id).Error
}

func (r *activitySessionRepository) ListUserSessions(userID uint) ([]*database.ActivitySession, error) {
	var sessions []*database.ActivitySession
	if err := r.db.Where("user_id = ?", userID).Preload("Location").Find(&sessions).Error; err != nil {
		return nil, err
	}
	return sessions, nil
}

// ==== ActivityRecordRepository ====
type ActivityRecordRepository interface {
	Create(record *database.ActivityRecord) (*database.ActivityRecord, error)
	BulkCreate(records []*database.ActivityRecord) error
	GetByID(id uint) (*database.ActivityRecord, error)
	GetBySessionID(sessionID uint) ([]*database.ActivityRecord, error)
	GetSampledRecordsBySessionID(sessionID uint) ([]*database.ActivityRecord, error)
	Update(record *database.ActivityRecord) error
	Delete(id uint) error
	DeleteBySessionID(sessionID uint) error
	List() ([]*database.ActivityRecord, error)
}

type activityRecordRepository struct {
	db *gorm.DB
}

func (r *activityRecordRepository) GetSampledRecordsBySessionID(sessionID uint) ([]*database.ActivityRecord, error) {
	var records []*database.ActivityRecord
	if err := r.db.
		Where("session_id = ?", sessionID).
		Where("position_lat != 0 AND position_long != 0").
		Find(&records).
		Order("timestamp").Error; err != nil {
		return nil, err
	}
	var sampled []*database.ActivityRecord
	for i := 0; i < len(records); i = i + 10 {
		sampled = append(sampled, records[i])
	}
	return sampled, nil
}

// Constructor for ActivityRecordRepository
func NewActivityRecordRepository(db *gorm.DB) ActivityRecordRepository {
	return &activityRecordRepository{db: db}
}

func (r *activityRecordRepository) Create(record *database.ActivityRecord) (*database.ActivityRecord, error) {
	if err := r.db.Create(record).Error; err != nil {
		return nil, err
	}
	return record, nil
}

func (r *activityRecordRepository) BulkCreate(records []*database.ActivityRecord) error {
	return r.db.CreateInBatches(&records, 100).Error
}

func (r *activityRecordRepository) GetByID(id uint) (*database.ActivityRecord, error) {
	var record database.ActivityRecord
	if err := r.db.First(&record, id).Error; err != nil {
		return nil, err
	}
	return &record, nil
}

func (r *activityRecordRepository) GetBySessionID(sessionID uint) ([]*database.ActivityRecord, error) {
	var records []*database.ActivityRecord
	if err := r.db.
		Where("session_id = ?", sessionID).
		Find(&records).
		Order("timestamp").Error; err != nil {
		return nil, err
	}
	return records, nil
}

func (r *activityRecordRepository) Update(record *database.ActivityRecord) error {
	return r.db.Save(record).Error
}

func (r *activityRecordRepository) Delete(id uint) error {
	return r.db.Delete(&database.ActivityRecord{}, id).Error
}

func (r *activityRecordRepository) DeleteBySessionID(sessionID uint) error {
	return r.db.Where("session_id = ?", sessionID).Delete(&database.ActivityRecord{}).Error
}

func (r *activityRecordRepository) List() ([]*database.ActivityRecord, error) {
	var records []*database.ActivityRecord
	if err := r.db.Find(&records).Error; err != nil {
		return nil, err
	}
	return records, nil
}

// ==== ActivitySetRepository ====
type ActivitySetRepository interface {
	Create() (*database.ActivitySet, error)
	GetByID(id uint) (*database.ActivitySet, error)
	/*
		BulkCreate(records []*database.ActivityRecord) error
		GetBySessionID(sessionID uint) ([]*database.ActivityRecord, error)
		Update(record *database.ActivityRecord) error
		Delete(id uint) error
		DeleteBySessionID(sessionID uint) error
		List() ([]*database.ActivityRecord, error)
	*/
}

type activitySetRepository struct {
	db *gorm.DB
}

func (r *activitySetRepository) GetByID(id uint) (*database.ActivitySet, error) {
	var activitySet database.ActivitySet
	if err := r.db.First(&activitySet, id).Error; err != nil {
		return nil, err
	}
	return &activitySet, nil
}

func (r *activitySetRepository) Create() (*database.ActivitySet, error) {
	var activitySet *database.ActivitySet
	if err := r.db.Create(&activitySet).Error; err != nil {
		return nil, err
	}
	return activitySet, nil
}

// Constructor for ActivitySetRepository
func NewActivitySetRepository(db *gorm.DB) ActivitySetRepository {
	return &activitySetRepository{db: db}
}
