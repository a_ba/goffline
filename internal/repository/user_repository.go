package repository

import (
	"goffline/internal/database"
	"gorm.io/gorm"
)

type UserRepository interface {
	Create(user *database.User) (*database.User, error)
	GetByID(id uint) (*database.User, error)
	GetByEmail(email string) (*database.User, error)
	Update(user *database.User) error
	Delete(id uint) error
	List() ([]*database.User, error)
}

type activityRepository struct {
	db *gorm.DB
}

// NewUserRepository creates a new instance of user repository
func NewUserRepository(db *gorm.DB) UserRepository {
	return &activityRepository{db: db}
}

func (r *activityRepository) Create(user *database.User) (*database.User, error) {
	if err := r.db.Create(user).Error; err != nil {
		return nil, err
	}
	return user, nil
}

func (r *activityRepository) GetByID(id uint) (*database.User, error) {
	var user database.User
	if err := r.db.First(&user, id).Error; err != nil {
		return nil, err
	}
	return &user, nil
}

func (r *activityRepository) GetByEmail(email string) (*database.User, error) {
	var user database.User
	if err := r.db.Where("email = ?", email).First(&user).Error; err != nil {
		return nil, err
	}
	return &user, nil
}

func (r *activityRepository) Update(user *database.User) error {
	return r.db.Save(user).Error
}

func (r *activityRepository) Delete(id uint) error {
	return r.db.Delete(&database.User{}, id).Error
}

func (r *activityRepository) List() ([]*database.User, error) {
	var users []*database.User
	if err := r.db.Find(&users).Error; err != nil {
		return nil, err
	}
	return users, nil
}
