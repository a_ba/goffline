package server

import (
	"github.com/gin-gonic/gin"
	"goffline/internal/services"
	"net/http"
	"strconv"
)

type ActivityHandler struct {
	activityService services.ActivityService
}

func NewActivityHandler(activityService services.ActivityService) *ActivityHandler {
	return &ActivityHandler{activityService: activityService}
}

func (h *ActivityHandler) ImportActivities(c *gin.Context) {
	importCount, err := h.activityService.ImportActivities(1)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "User not found"})
	}
	c.JSON(http.StatusOK, gin.H{
		"message":    "Import success",
		"file_count": importCount,
	})
}

func (h *ActivityHandler) ListUserSessions(c *gin.Context) {
	sessions, err := h.activityService.ListUserSessions(1)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	c.JSON(http.StatusOK, sessions)
}

func (h *ActivityHandler) GetDetailsByID(c *gin.Context) {
	id, _ := strconv.Atoi(c.Param("id"))

	activityDetails, err := h.activityService.GetDetailsByID(uint(id))
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": "User not found"})
		return
	}

	c.JSON(http.StatusOK, &activityDetails)
}
