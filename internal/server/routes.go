package server

import (
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

func (s *Server) registerRoutes() *gin.Engine {
	r := gin.Default()

	r.Use(cors.New(cors.Config{
		AllowOrigins:     []string{"http://localhost:5173"}, // Add your frontend URL
		AllowMethods:     []string{"GET", "POST", "PUT", "DELETE", "OPTIONS", "PATCH"},
		AllowHeaders:     []string{"Accept", "Authorization", "Content-Type"},
		AllowCredentials: true, // Enable cookies/auth
	}))

	//userHandler := NewUserHandler(s.userService)
	nextCloudHandler := NewNextCloudHandler(s.nextCloudService)
	activityHandler := NewActivityHandler(s.activityService)
	authHandler := NewAuthHandler(s.userService)

	api := r.Group("/api")

	settingsGroup := api.Group("/settings/nextcloud").Use(AuthMiddleware())
	{
		settingsGroup.GET("", nextCloudHandler.GetByUserID)
		settingsGroup.POST("", nextCloudHandler.Create)
		settingsGroup.PUT("", nextCloudHandler.Update)
		settingsGroup.DELETE("", nextCloudHandler.Delete)
	}
	activitiesGroup := api.Group("/activities").Use(AuthMiddleware())
	{
		activitiesGroup.GET("", activityHandler.ListUserSessions)
		activitiesGroup.GET("/:id", activityHandler.GetDetailsByID)
		activitiesGroup.POST("/import", activityHandler.ImportActivities)
	}
	authGroup := api.Group("/auth")
	{
		authGroup.POST("/login", authHandler.Login)
		authGroup.POST("/refresh", authHandler.Refresh)
	}
	return r

}
