package server

import (
	"github.com/gin-gonic/gin"
	"goffline/internal/database"
	"goffline/internal/services"
	"net/http"
	"strconv"
)

type NextCloudHandler struct {
	nextCloudService services.NextCloudService
}

func NewNextCloudHandler(nextCloudService services.NextCloudService) *NextCloudHandler {
	return &NextCloudHandler{nextCloudService: nextCloudService}
}

func (h *NextCloudHandler) Create(c *gin.Context) {
	var config database.NextCloudConfig
	if err := c.ShouldBindJSON(&config); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	createdConfig, err := h.nextCloudService.Create(&config)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, createdConfig)
}

func (h *NextCloudHandler) GetByUserID(c *gin.Context) {
	userID := 1

	config, err := h.nextCloudService.GetByID(uint(userID))
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": "config not found"})
		return
	}

	c.JSON(http.StatusOK, config)
}

func (h *NextCloudHandler) Update(c *gin.Context) {
	var config database.NextCloudConfig
	if err := c.ShouldBindJSON(&config); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	if err := h.nextCloudService.Update(&config); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": "config updated successfully"})
}

func (h *NextCloudHandler) Delete(c *gin.Context) {
	id, _ := strconv.Atoi(c.Param("id"))

	if err := h.nextCloudService.Delete(uint(id)); err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": "config not found"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": "config deleted successfully"})
}

func (h *NextCloudHandler) List(c *gin.Context) {
	configs, err := h.nextCloudService.List()
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, configs)
}
