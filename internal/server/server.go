package server

import (
	"fmt"
	"goffline/internal/database"
	"goffline/internal/repository"
	"goffline/internal/services"
	"log"
	"net/http"
	"strconv"
	"time"
)

type Server struct {
	port int

	userService      services.UserService
	nextCloudService services.NextCloudService
	activityService  services.ActivityService
}

func NewServer() *http.Server {
	port, _ := strconv.Atoi("8080")
	//port, _ := strconv.Atoi(os.Getenv("PORT")) TODO: Check why ENV is not respected

	// Initialize database connection
	db, err := database.InitializeDatabase()
	if err != nil {
		log.Fatal("Error initializing database:", err)
	}

	// Initialize Repositories
	userRepo := repository.NewUserRepository(db)
	nextCloudRepo := repository.NewNextCloudRepository(db)
	activitySessionRepo := repository.NewActivitySessionRepository(db)
	activityRecordRepo := repository.NewActivityRecordRepository(db)
	activityLocationRepo := repository.NewActivityLocationRepository(db)
	activitySetRepo := repository.NewActivitySetRepository(db)

	// Initialize Services
	userService := services.NewUserService(userRepo)
	nextCloudService := services.NewNextCloudService(nextCloudRepo)
	activityService := services.NewActivityService(
		activitySessionRepo,
		activityRecordRepo,
		activityLocationRepo,
		nextCloudRepo,
		activitySetRepo)

	// Create server instance
	NewServer := &Server{
		port:             port,
		userService:      userService,
		nextCloudService: nextCloudService,
		activityService:  activityService,
	}

	// Register routes
	router := NewServer.registerRoutes() // Call the registerRoutes() from routes.go.

	// Declare Server config
	server := &http.Server{
		Addr:         fmt.Sprintf(":%d", NewServer.port),
		Handler:      router,
		IdleTimeout:  time.Minute,
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 30 * time.Second,
	}

	return server
}
