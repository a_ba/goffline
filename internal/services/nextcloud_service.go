package services

import (
	"github.com/studio-b12/gowebdav"
	"goffline/internal/database"
	"goffline/internal/repository"
	"io"
	"os"
	"path/filepath"
	"slices"
)

type NextCloudService interface {
	Create(config *database.NextCloudConfig) (*database.NextCloudConfig, error)
	GetByID(id uint) (*database.NextCloudConfig, error)
	GetByUserID(userID uint) (*database.NextCloudConfig, error)
	VerifyNextCloudCredentials(host, token, password string) error
	Update(config *database.NextCloudConfig) error
	Delete(id uint) error
	List() ([]*database.NextCloudConfig, error)
}

type nextCloudService struct {
	repo repository.NextCloudRepository
}

func (s *nextCloudService) GetByUserID(userID uint) (*database.NextCloudConfig, error) {
	return s.repo.GetByUserID(userID)
}

func NewNextCloudService(repo repository.NextCloudRepository) NextCloudService {
	return &nextCloudService{repo: repo}
}

func (s *nextCloudService) VerifyNextCloudCredentials(host, token, password string) error {
	host = host + "/public.php/webdav"
	client := gowebdav.NewClient(host, token, password)
	err := client.Connect()
	if err != nil {
		return ErrInvalidCredentials
	}
	return nil
}

func (s *nextCloudService) Create(config *database.NextCloudConfig) (*database.NextCloudConfig, error) {
	return s.repo.Create(config)
}

func (s *nextCloudService) GetByID(id uint) (*database.NextCloudConfig, error) {
	return s.repo.GetByID(id)
}

func (s *nextCloudService) Update(config *database.NextCloudConfig) error {
	return s.repo.Update(config)
}

func (s *nextCloudService) Delete(id uint) error {
	return s.repo.Delete(id)
}

func (s *nextCloudService) List() ([]*database.NextCloudConfig, error) {
	return s.repo.List()
}

func getNextCloudClient(config *database.NextCloudConfig) (*gowebdav.Client, error) {
	client := gowebdav.NewClient(config.Host+"/public.php/webdav", config.Token, config.Password)
	err := client.Connect()
	if err != nil {
		return nil, err
	}
	return client, nil
}
func downloadNextCloudFiles(config *database.NextCloudConfig, basePath string, excludedFiles []string) error {
	client, err := getNextCloudClient(config)
	if err != nil {
		return err
	}
	remoteFiles, _ := client.ReadDir("")
	for _, remoteFile := range remoteFiles {
		// Check if file has already been imported
		if slices.Contains(excludedFiles, remoteFile.Name()) {
			continue
		}
		fPathLocal := filepath.Join(basePath, "fitfiles", remoteFile.Name())
		reader, _ := client.ReadStream(remoteFile.Name())

		file, _ := os.Create(fPathLocal)
		defer file.Close()

		_, copyErr := io.Copy(file, reader)
		if copyErr != nil {
			return copyErr
		}
	}
	return nil

}
