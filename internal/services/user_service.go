package services

import (
	"goffline/internal/database"
	"goffline/internal/repository"
)

type UserService interface {
	Create(user *database.User) (*database.User, error)
	GetByID(id uint) (*database.User, error)
	CheckUserCredentials(email, password string) (*database.User, error)
	Update(user *database.User) error
	Delete(id uint) error
	List() ([]*database.User, error)
}

type userService struct {
	repo repository.UserRepository
}

func NewUserService(repo repository.UserRepository) UserService {
	return &userService{repo: repo}
}

func (s *userService) CheckUserCredentials(email, password string) (*database.User, error) {
	user, err := s.repo.GetByEmail(email)
	if err != nil {
		return nil, ErrInvalidCredentials
	}
	if user.Password != password {
		return nil, ErrInvalidCredentials
	}
	return user, nil
}

func (s *userService) Create(user *database.User) (*database.User, error) {
	return s.repo.Create(user)
}

func (s *userService) GetByID(id uint) (*database.User, error) {
	return s.repo.GetByID(id)
}

func (s *userService) Update(user *database.User) error {
	return s.repo.Update(user)
}

func (s *userService) Delete(id uint) error {
	return s.repo.Delete(id)
}

func (s *userService) List() ([]*database.User, error) {
	return s.repo.List()
}
