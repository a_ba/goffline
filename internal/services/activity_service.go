package services

import (
	"fmt"
	"github.com/codingsince1985/geo-golang/openstreetmap"
	"github.com/muktihari/fit/decoder"
	"github.com/muktihari/fit/profile/filedef"
	"github.com/muktihari/fit/profile/mesgdef"
	"goffline/internal/database"
	"goffline/internal/repository"
	"math"
	"os"
	"path/filepath"
	"strings"
)

type ActivityDetails struct {
	Session           *database.ActivitySession   `json:"session"`
	Records           []*database.ActivityRecord  `json:"records"`
	MatchedActivities []*database.ActivitySession `json:"matched_activities"`
}
type ActivityService interface {
	GetByID(id uint) (*database.ActivitySession, error)
	GetDetailsByID(id uint) (*ActivityDetails, error)
	ListUserSessions(userID uint) ([]*database.ActivitySession, error)
	ImportActivities(userID uint) (fileCount uint, err error)
	ActivityMatcher(userID uint) error
}

type activityService struct {
	sessionRepo     repository.ActivitySessionRepository
	recordRepo      repository.ActivityRecordRepository
	locationRepo    repository.ActivityLocationRepository
	nextCloudRepo   repository.NextCloudRepository
	activitySetRepo repository.ActivitySetRepository
}

func (s *activityService) GetDetailsByID(id uint) (*ActivityDetails, error) {

	var activityDetails ActivityDetails
	activitySession, err := s.sessionRepo.GetByID(id)
	if err != nil {
		return nil, err
	}
	records, err := s.recordRepo.GetSampledRecordsBySessionID(id)
	if err != nil {
		return nil, err
	}
	matchedActivities, err := s.sessionRepo.GetMatchedActivities(id)
	if err != nil {
		return nil, err
	}

	activityDetails.Session = activitySession
	activityDetails.Records = records
	activityDetails.MatchedActivities = matchedActivities

	return &activityDetails, nil

}

func NewActivityService(
	sessionRepo repository.ActivitySessionRepository,
	recordRepo repository.ActivityRecordRepository,
	locationRepo repository.ActivityLocationRepository,
	nextCloudRepo repository.NextCloudRepository,
	activitySetRepo repository.ActivitySetRepository) ActivityService {
	return &activityService{
		sessionRepo:     sessionRepo,
		recordRepo:      recordRepo,
		locationRepo:    locationRepo,
		nextCloudRepo:   nextCloudRepo,
		activitySetRepo: activitySetRepo,
	}
}

func (s *activityService) GetByID(id uint) (*database.ActivitySession, error) {
	activitySession, err := s.sessionRepo.GetByID(id)
	if err != nil {
		return nil, err
	}
	records, err := s.recordRepo.GetSampledRecordsBySessionID(id)
	activitySession.Records = records

	return activitySession, nil
}

func (s *activityService) ListUserSessions(userID uint) ([]*database.ActivitySession, error) {
	return s.sessionRepo.ListUserSessions(userID)
}

func (s *activityService) getImportedUserFiles(userID uint) (fileNames []string, err error) {
	sessions, err := s.ListUserSessions(userID)
	if err != nil {
		return nil, err
	}
	fileNames = make([]string, len(sessions))
	for _, session := range sessions {
		fileNames = append(fileNames, session.Filename)
	}
	return fileNames, nil
}

func (s *activityService) downloadFitFiles(userID uint, basePath string, oldUserFiles []string) error {
	nextCloudConfig, err := s.nextCloudRepo.GetByUserID(userID)
	if err != nil {
		return err
	}
	err = downloadNextCloudFiles(nextCloudConfig, basePath, oldUserFiles)
	if err != nil {
		return err
	}
	return nil
}

func (s *activityService) parseActivities(userID uint, basePath string) (fileCount uint, err error) {
	inputDir := filepath.Join(basePath, "fitfiles")
	files, err := os.ReadDir(inputDir)
	if err != nil {
		return 0, err
	}
	importCount := 0
	for _, file := range files {
		if file.IsDir() {
			continue
		}
		if !strings.HasSuffix(file.Name(), ".FIT") {
			continue
		}

		filePath := filepath.Join(inputDir, file.Name())

		f, err := os.Open(filePath)
		if err != nil {
			return 0, err
		}
		defer f.Close()

		// The listener will receive every decoded message from the decoder as soon as it is decoded
		// and transform it into an filedef.File.
		lis := filedef.NewListener()
		defer lis.Close() // release channel used by listener

		dec := decoder.New(f,
			decoder.WithMesgListener(lis), // Add activity listener to the decoder
			decoder.WithBroadcastOnly(),   // Direct the decoder to only broadcast the messages without retaining them.
		)

		_, err = dec.Decode()
		if err != nil {
			panic(err)
		}

		// The resulting File can be retrieved after decoding process completed.
		// filedef.File is just an interface, we can do type assertion like this.
		activityFile, _ := lis.File().(*filedef.Activity)

		session := parseFITSession(activityFile.Sessions[0])
		session.Filename = file.Name()
		session.UserID = userID
		// Save Session data
		if _, err := s.sessionRepo.Create(session); err != nil {
			fmt.Println(err)
			continue

		}
		var records []*database.ActivityRecord
		for _, record := range activityFile.Records {
			r := parseFITRecord(record)
			r.SessionID = session.ID
			records = append(records, r)
		}

		err = s.recordRepo.BulkCreate(records)
		if err != nil {
			fmt.Println(err)
			continue
		}

		// move processed file
		processedFilePath := filepath.Join(basePath, "/processed/", file.Name())
		if err := os.Rename(filePath, processedFilePath); err != nil {
			fmt.Println(err)
		}

		importCount++
	}

	return uint(importCount), nil
}

func (s *activityService) ImportActivities(userID uint) (fileCount uint, err error) {
	basePath := "/app/data"
	oldUserFiles, err := s.getImportedUserFiles(userID)
	if err != nil {
		return 0, err
	}

	err = s.downloadFitFiles(userID, basePath, oldUserFiles)
	if err != nil {
		return 0, err
	}

	fileCount, err = s.parseActivities(userID, basePath)
	if err != nil {
		return 0, err
	}
	err = s.ActivityMatcher(userID)
	if err != nil {
		return fileCount, err
	}
	err = s.GeoCodeActivity(userID)
	if err != nil {
		return fileCount, err
	}
	return fileCount, nil
}

func parseFITRecord(fitRecord *mesgdef.Record) *database.ActivityRecord {
	record := &database.ActivityRecord{}
	record.Timestamp = fitRecord.Timestamp
	record.PositionLat = fitRecord.PositionLatDegrees()
	record.PositionLong = fitRecord.PositionLongDegrees()

	if math.IsNaN(record.PositionLat) {
		record.PositionLat = 0
	}
	if math.IsNaN(record.PositionLong) {
		record.PositionLong = 0
	}

	record.Distance = fitRecord.DistanceScaled()
	record.Altitude = fitRecord.AltitudeScaled()
	record.Speed = fitRecord.SpeedScaled()
	record.HeartRate = fitRecord.HeartRate
	record.Cadence = fitRecord.Cadence
	record.FractionalCadence = fitRecord.FractionalCadenceScaled()
	record.EnhancedSpeed = fitRecord.EnhancedSpeedScaled()
	record.EnhancedAltitude = fitRecord.EnhancedAltitudeScaled()
	return record
}

func parseFITSession(fitSession *mesgdef.Session) *database.ActivitySession {
	session := &database.ActivitySession{}
	session.Timestamp = fitSession.Timestamp
	session.StartTime = fitSession.StartTime
	session.SportProfileName = fitSession.SportProfileName

	session.StartPositionLat = fitSession.StartPositionLatDegrees()
	if math.IsNaN(session.StartPositionLat) {
		session.StartPositionLat = 0
	}

	session.StartPositionLong = fitSession.StartPositionLongDegrees()
	if math.IsNaN(session.StartPositionLong) {
		session.StartPositionLong = 0
	}
	session.TotalElapsedTime = fitSession.TotalElapsedTimeScaled()
	session.TotalTimerTime = fitSession.TotalTimerTimeScaled()
	session.TotalDistance = fitSession.TotalDistanceScaled()
	session.TotalCycles = fitSession.TotalCycles
	session.MessageIndex = fitSession.MessageIndex
	session.TotalCalories = fitSession.TotalCalories
	session.AvgSpeed = fitSession.AvgSpeedScaled()
	session.MaxSpeed = fitSession.MaxSpeedScaled()
	session.TotalAscent = fitSession.TotalAscent
	session.TotalDescent = fitSession.TotalDescent
	session.FirstLapIndex = fitSession.FirstLapIndex
	session.NumLaps = fitSession.NumLaps
	session.Event = fitSession.Event
	session.EventType = fitSession.EventType
	session.Sport = fitSession.Sport
	session.SubSport = fitSession.SubSport
	session.AvgHeartRate = fitSession.AvgHeartRate
	session.MaxHeartRate = fitSession.MaxHeartRate
	session.AvgCadence = fitSession.AvgCadence
	session.MaxCadence = fitSession.MaxCadence
	session.AvgFractionalCadence = fitSession.AvgFractionalCadenceScaled()
	session.MaxFractionalCadence = fitSession.AvgFractionalCadenceScaled()

	return session
}

func euclideanDistance(record1, record2 *database.ActivityRecord) float64 {
	// Calculate differences
	latDiff := record1.PositionLat - record2.PositionLat
	lonDiff := record1.PositionLong - record2.PositionLong

	// Euclidean distance
	return math.Sqrt(latDiff*latDiff + lonDiff*lonDiff)
}

func frechetDistance(expData, numData []*database.ActivityRecord) float64 {
	n := len(expData)
	m := len(numData)

	// Calculate distance matrix
	c := make([][]float64, n)
	for i := range c {
		c[i] = make([]float64, m)
		for j := range c[i] {
			c[i][j] = euclideanDistance(expData[i], numData[j])
		}
	}

	// Create cost accumulation matrix
	ca := make([][]float64, n)
	for i := range ca {
		ca[i] = make([]float64, m)
		for j := range ca[i] {
			ca[i][j] = -1
		}
	}

	// Initialize first cell
	ca[0][0] = c[0][0]

	// Fill first column
	for i := 1; i < n; i++ {
		ca[i][0] = math.Max(ca[i-1][0], c[i][0])
	}

	// Fill first row
	for j := 1; j < m; j++ {
		ca[0][j] = math.Max(ca[0][j-1], c[0][j])
	}

	// Fill the rest of the matrix
	for i := 1; i < n; i++ {
		for j := 1; j < m; j++ {
			ca[i][j] = math.Max(
				math.Min(math.Min(ca[i-1][j], ca[i][j-1]), ca[i-1][j-1]),
				c[i][j],
			)
		}
	}

	return ca[n-1][m-1]
}

func (s *activityService) matchActivity(activity *database.ActivitySession) (*database.ActivitySet, error) {
	matchedActivities, err := s.sessionRepo.UserSessionsWithActivitySet(activity.UserID)
	if err != nil {
		return nil, err
	}

	// First iteration for user -> no previously matched activities for user
	if len(matchedActivities) == 0 {
		return s.activitySetRepo.Create()
	}

	// At least one previously matched activity - start comparison
	for _, activityCompare := range matchedActivities {
		fmt.Printf("Comparing Activity %d with %d", activity.ID, activityCompare.ID)
		// Check if total distance is similar (+-2%)
		if math.Abs(activity.TotalDistance/activityCompare.TotalDistance-1) > 0.025 {
			fmt.Println("Distance dissimilar")
			continue
		}

		activityRecords, err := s.recordRepo.GetSampledRecordsBySessionID(activity.ID)
		if err != nil {
			return nil, err
		}
		activityCompareRecords, err := s.recordRepo.GetSampledRecordsBySessionID(activityCompare.ID)
		if err != nil {
			return nil, err
		}

		// Check if starting position is similar
		if euclideanDistance(activityRecords[0], activityCompareRecords[0]) > 120 {
			fmt.Println("Starting Position dissimilar")
			continue
		}

		// Starting positions and distances similar -> compare tracks
		distance := frechetDistance(activityRecords, activityCompareRecords)
		if distance < 0.015 {
			return s.activitySetRepo.GetByID(activityCompare.ActivitySetID)
		}
		fmt.Printf("Track dissimilar. Frechet distance: %.6f", distance)
	}

	// Could not find matching activity set in candidate list -> create a new one
	return s.activitySetRepo.Create()

}

func (s *activityService) ActivityMatcher(userID uint) error {
	fmt.Println("Start ActivityMatcher")
	unmatchedActivities, err := s.sessionRepo.UserSessionsWithoutActivitySet(userID)
	if err != nil {
		return err
	}
	fmt.Printf("Found %d unmatched activities\n", len(unmatchedActivities))
	for _, activity := range unmatchedActivities {

		activitySet, err := s.matchActivity(activity)
		if err != nil {
			return err
		}
		activity.ActivitySetID = activitySet.ID
		err = s.sessionRepo.Update(activity)
		if err != nil {
			return err
		}
	}

	return nil
}

func (s *activityService) findClosestLocation(session *database.ActivitySession) (*database.ActivityLocation, error) {
	proximateLocations, err := s.locationRepo.FindLocationsInRadius(session.StartPositionLat, session.StartPositionLong, 300.)
	if err != nil {
		return nil, err
	}
	if len(proximateLocations) == 0 {
		return nil, nil
	}

	// Find the closest location using Euclidean distance
	var sessionRecord = database.ActivityRecord{PositionLat: session.StartPositionLat, PositionLong: session.StartPositionLong}
	var recordCompare = database.ActivityRecord{PositionLat: proximateLocations[0].Latitude, PositionLong: proximateLocations[0].Longitude}
	closestLocation := proximateLocations[0]
	closestDistance := euclideanDistance(
		&sessionRecord,
		&recordCompare,
	)

	for i := 1; i < len(proximateLocations); i++ {
		recordCompare = database.ActivityRecord{PositionLat: proximateLocations[i].Latitude, PositionLong: proximateLocations[i].Longitude}
		distance := euclideanDistance(
			&sessionRecord,
			&recordCompare,
		)
		if distance < closestDistance {
			closestDistance = distance
			closestLocation = proximateLocations[i]
		}
	}

	// If closest location is further than our maximum distance, return nil
	if closestDistance > 200 {
		return nil, nil
	}

	return closestLocation, err

}
func (s *activityService) reverseGeoCodeSessionLocation(session *database.ActivitySession) (*database.ActivityLocation, error) {
	address, err := openstreetmap.Geocoder().ReverseGeocode(
		session.StartPositionLat,
		session.StartPositionLong)
	if err != nil {
		return nil, err
	}

	location, err := s.locationRepo.Create(
		&database.ActivityLocation{
			SessionID:     session.ID,
			Latitude:      session.StartPositionLat,
			Longitude:     session.StartPositionLong,
			Suburb:        address.Suburb,
			Postcode:      address.Postcode,
			State:         address.State,
			StateCode:     address.StateCode,
			StateDistrict: address.StateDistrict,
			County:        address.County,
			Country:       address.Country,
			CountryCode:   address.CountryCode,
			City:          address.City})
	if err != nil {
		return nil, err
	}
	return location, nil
}

func (s *activityService) GeoCodeActivity(userID uint) error {
	sessions, err := s.sessionRepo.UserSessionsWithoutLocation(userID)
	if err != nil {
		return err
	}

	for _, session := range sessions {
		closestLocation, err := s.findClosestLocation(session)
		if err != nil {
			return err
		}
		if closestLocation == nil {
			location, err := s.reverseGeoCodeSessionLocation(session)
			if err != nil {
				return err
			}
			session.Location = location
		} else {
			session.Location = closestLocation
		}
		err = s.sessionRepo.Update(session)
		if err != nil {
			return err
		}
	}

	return nil
}
