import LoginPage from './pages/login';
import {AppLayout, AuthLayout} from './components/layout';
import {createBrowserRouter, createRoutesFromElements, Route, RouterProvider} from "react-router-dom";
import Dashboard from './pages/dashboard';
import ActivityDetail from './pages/details';

import axios from 'axios';
import {QueryClient, QueryClientProvider} from "@tanstack/react-query";
import SettingsPage from "./pages/settingsPage.jsx";

axios.defaults.baseURL = `${import.meta.env.VITE_API_URL}`
axios.interceptors.request.use(
  (config) => {
    const token = localStorage.getItem('token');
    if (token && token !== "null") {
      let tokenObj = JSON.parse(token)
      config.headers.Authorization = `Bearer ${tokenObj.access_token}`;
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

// Define a default query function that will receive the query key
const defaultQueryFn = async ({ queryKey }) => {
    const { data } = await axios.get(`${queryKey[0]}`,)
    return data
}

// provide the default query function to your app with defaultOptions
const queryClient = new QueryClient({
    defaultOptions: {
        queries: {
            queryFn: defaultQueryFn,
        },
    },
})

const router = createBrowserRouter(
  createRoutesFromElements(
    <>
      <Route element={<AuthLayout />}>
        <Route path="/login" element={<LoginPage />} />
        <Route path="/"  element={<AppLayout />}>
          <Route index element={<Dashboard />} />
            <Route path="/settings" element={<SettingsPage />} />
          <Route path="session/:sessionId" element={<ActivityDetail />} />
        </Route>
      </Route>
    </>
  )
)


const App = () => {

  return (
    <div>
        <QueryClientProvider client={queryClient}>
          <RouterProvider router={router} />
        </QueryClientProvider>
    </div>

  )
};

export default App
