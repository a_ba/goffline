import {Breadcrumb, Layout, Menu, notification, Spin, theme} from 'antd';
import {Link, Navigate, Outlet, useNavigate, useOutlet} from "react-router-dom";
import gofflineLogo from '../assets/goffline_no_bg.svg'
import {AuthProvider, useAuth} from "../hooks/useAuth";
import {ImportOutlined, LoadingOutlined, LogoutOutlined, SettingOutlined,} from '@ant-design/icons';
import axios from "axios";
import {useMutation, useQueryClient} from "@tanstack/react-query";


const {Header, Content, Footer} = Layout;
export const AppLayout = () => {
    const {token: {colorBgContainer, borderRadiusLG},} = theme.useToken();
    const {logout, token} = useAuth();
    const queryClient = useQueryClient()
    const navigate = useNavigate();
    const mutation = useMutation({
        mutationFn: async () => {
            try{
                let result =  await axios.post('/activities/import', {})
                notification.success({
                    message: 'Import Success',
                    showProgress: true,
                    description: result.data.message
                });
                return result
            }catch(e){

            }
        },
        onSuccess: () => {
            queryClient.invalidateQueries({ queryKey: ['activities'] })
        },
    })

    if (!token) {
        return <Navigate to="/login"/>;
    }

    const menuItems = [
        {
            label: 'Log Out',
            key: 'logout',
            style: {float: 'right'},
            onClick: logout,
            icon: <LogoutOutlined/>,
        },
        {
            label: 'Settings',
            key: 'settings',
            style: {float: 'right'},
            onClick: () => {    navigate("/settings")},
            icon: <SettingOutlined/>,
        },
        {
            label: 'Import Activities',
            key: 'import',
            style: {float: 'right'},
            onClick: () => mutation.mutate({}),
            disabled: mutation.isPending,
            icon: mutation.isPending ? <Spin indicator={<LoadingOutlined spin/>}/> : <ImportOutlined/>

        },
    ]


    return (
        <Layout style={{minHeight: "100vh"}}>
            <Header
                style={{
                    display: 'flex',
                    alignItems: 'center',
                }}
            >
                <Link to={"/"} style={{lineHeight: "0px"}}>
                    <img src={gofflineLogo} alt="Goffline logo"/>
                </Link>

                <Menu

                    theme="dark"
                    mode="horizontal"
                    selectable={false}
                    style={{
                        display: 'block',
                        flex: 1,
                        minWidth: 0,
                    }}
                    items={menuItems}
                />

            </Header>
            <Content
                style={{
                    padding: '0 48px',
                }}
            >
                <Breadcrumb
                    style={{
                        margin: '16px 0',
                    }}

                    items={[]}
                />
                <div
                    style={{
                        background: colorBgContainer,
                        minHeight: 280,
                        padding: 24,
                        borderRadius: borderRadiusLG,
                    }}
                >
                    <AuthProvider>
                        <Outlet/>
                    </AuthProvider>
                </div>
            </Content>
            <Footer
                style={{
                    textAlign: 'center',
                }}
            >
                GOffline {new Date().getFullYear()}
            </Footer>
        </Layout>
    );
};


export const AuthLayout = () => {
    const outlet = useOutlet();

    return (
        <AuthProvider>{outlet}</AuthProvider>
    );
};
