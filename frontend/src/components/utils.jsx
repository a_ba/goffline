
function dateFromFitDateTime(timestamp) {
  // Parse the ISO timestamp into a Date object
  const date = new Date(timestamp);

// Define options for formatting the date
  const options = { day: 'numeric', month: 'short', year: 'numeric' };

// Format the date as desired
  return date.toLocaleString('en-GB', options);
}

function dateFormatter(timestamp) {
  var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
  var today  = new Date(timestamp);
  return today.toLocaleDateString("en-US", options)
}
function timeOfDayFormatter(timestamp) {
  // Determine the time of day
  let date = new Date(timestamp);
  let timeOfDay;
  let hours = date.getHours();
  if (hours >= 5 && hours < 12) {
    timeOfDay = "Morning";
  } else if (hours >= 12 && hours < 15) {
    timeOfDay = "Midday";
  } else if (hours >= 15 && hours < 18) {
    timeOfDay = "Afternoon";
  } else if (hours >= 18 && hours < 21) {
    timeOfDay = "Evening";
  } else {
    timeOfDay = "Night";
  }

  return timeOfDay + " Run"
}


function convertTimestampToHMS(timestamp) {
  return new Intl.DateTimeFormat('de', {
    //year: 'numeric',
    //day: '2-digit',
    // month: '2-digit',
    hour: '2-digit',
    minute: '2-digit',
    second: '2-digit',
  }).format(new Date(timestamp));
}


function convertMetersToKilometers(meters) {
  const kilometers = (meters / 1000).toFixed(1); // Convert meters to kilometers and round to 1 decimal place
  return `${kilometers} km`;
}

function roundFirstDecimal(number) {
  return Math.round(number * 10) / 10
}


function convertSecondsToHMS(seconds) {
  var hours = Math.floor(seconds / 3600);
  var minutes = Math.floor((seconds % 3600) / 60);
  var remainingSeconds = Math.round(seconds % 60);

  var displayHours = hours < 10 ? "0" + hours : hours;
  var displayMinutes = minutes < 10 ? "0" + minutes : minutes;
  var displaySeconds = remainingSeconds < 10 ? "0" + remainingSeconds : remainingSeconds;

  return displayHours + ":" + displayMinutes + ":" + displaySeconds;
}

function displayLocation(location) {
  if (!location) return "Unknown Location"
  const order = ["Suburb", "City", "State", "Country"];
  const resultArray = [];

  order.forEach(key => {
    if (location[key] !== "") {
      resultArray.push(location[key]);
    }
  });

  if (resultArray.length === 0) return "Unknown Location"
  return resultArray.slice(0,2).join(", ")


}
export {
  dateFromFitDateTime,
  convertMetersToKilometers,
  convertSecondsToHMS,
  convertTimestampToHMS,
  roundFirstDecimal,
  displayLocation,
  timeOfDayFormatter,
  dateFormatter
}
