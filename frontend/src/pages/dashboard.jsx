import {useNavigate} from "react-router-dom";
import {Card, Col, Divider, Empty, Flex, List, Progress, Row, Skeleton, Space, Statistic} from 'antd';
import {convertSecondsToHMS, dateFormatter, displayLocation, timeOfDayFormatter} from "../components/utils";
import {useQuery} from "@tanstack/react-query";
import HeatMap from "@uiw/react-heat-map";
import {
    DoubleRightOutlined,
    FieldTimeOutlined,
    HeartOutlined,
    UpCircleTwoTone,
} from "@ant-design/icons";
import {useState} from "react";

const ActivityHeatmap = props => {
    const {status, data, error, isFetching} = useQuery({
        queryKey: [`activities/heatmap`],
    })

    function getDateLastYear() {
        var today = new Date();
        var pastDate = new Date(today);
        return pastDate.setDate(today.getDate() - 365);
    }


    if (isFetching) return <div>Loading...</div>
    if (error) return <div>Error: {error.message}</div>;
    return (
        <Flex justify="center" align={"center"}>

            {isFetching ? (
                    <Skeleton active/>) :
                (<HeatMap
                        value={data}
                        width={800}
                        weekLabels={["Sun", "", "Tue", "", "Thu", "", "Sat"]}
                        legendCellSize={"Hide"}
                        startDate={new Date(getDateLastYear())}
                    />
                )
            }
        </Flex>
    )
}


const MonthlyProgressCard = props => {
    const twoColors = {
        '0%': '#108ee9',
        '100%': '#87d068',
    };
    return (
        <Row justify="space-around" gutter={[0,16]}>
            <Col span={8}>
                <Flex justify="center" align={"center"}>
                <Progress
                    type="circle"
                    percent={99}
                    strokeColor={twoColors}
                    status="active"
                />
                </Flex>
            </Col>
            <Col span={8}>
                <Flex justify="center" align={"center"}>
                    <Flex vertical={true} gap={"middle"}>
                        <Space>
                            CW XX
                            <Progress percent={0} steps={3}/>
                        </Space>
                        <Space>
                            CW XX
                            <Progress percent={0} steps={3}/>
                        </Space>
                        <Space>
                            CW XX
                            <Progress percent={0} steps={3}/>
                        </Space>
                        <Space>
                            CW XX
                            <Progress percent={0} steps={3}/>
                        </Space>

                    </Flex>
                </Flex>
            </Col>
            <Col span={8}>
                <Flex justify="center" align={"center"}>
                    <UpCircleTwoTone style={{fontSize: '120px'}} twoToneColor={"rgba(131,208,102,0.45)"}/>
                </Flex>
            </Col>
            <Col span={8}>
                <Flex justify="center" align={"center"}>
                    <h4>Distance</h4>
                </Flex>
            </Col>
            <Col span={8}>
                <Flex justify="center" align={"center"}>
                    <h4>Streak</h4>
                </Flex>
            </Col>
            <Col span={8}>
                <Flex justify="center" align={"center"}>
                    <h4>Form</h4>
                </Flex>
            </Col>
        </Row>
    )
}

const ActivityCard = props => {
    const navigate = useNavigate();
    if (props.loading) return <Skeleton active/>
    let item = props.item;
    return (
        <div onClick={() => {
            navigate("/session/" + item.ID)
        }}
             style={{width: "100%"}}
        >
            <Card
                size="small"
                style={{width: "100%"}}
                bordered={false}
                hoverable={true}
                actions={[
                    <Statistic key={"duration"} title="Duration" valueStyle={{fontSize: 16}}
                               value={convertSecondsToHMS(item.total_elapsed_time)}
                               prefix={<FieldTimeOutlined style={{fontSize: '16px'}}/>}/>,
                    <Statistic key={"distance"} title="Distance" valueStyle={{fontSize: 16}}
                               value={item.total_distance / 1000} precision={2}
                               prefix={<DoubleRightOutlined style={{fontSize: '16px'}}/>}/>,
                    <Statistic key={"heartRate"} title="Heart-Rate" valueStyle={{fontSize: 16}}
                               value={item.avg_heart_rate} prefix={<HeartOutlined style={{fontSize: '16px'}}/>}/>,

                ]}
            >
                <Card.Meta
                    title={timeOfDayFormatter(item.start_time)}
                    description={`${dateFormatter(item.start_time)}, ${displayLocation(item.location)}`}
                >
                </Card.Meta>
            </Card>
        </div>
    )
}

const ActivityList = props => {
    const [listData, setListData] = useState([]);
    const data = props.data;
    if (props.loading) return <Skeleton active/>;
    if (data.length === 0) return <Empty image={Empty.PRESENTED_IMAGE_SIMPLE}/>
    return (
        <div
            id="scrollableDiv"
            style={{
                height: 'calc(100vh - 265px)',
                //height: 'calc(100vh - 570px)',
                overflow: 'auto',
                border: 'none',
                scrollbarWidth: 'none', // Firefox
                '&::-webkit-scrollbar': {
                    display: 'none' // Chrome, Safari, Edge
                }
            }}
        >
            <List
                itemLayout="horizontal"
                dataSource={data}
                renderItem={(item, index) => (
                    <List.Item>
                        <ActivityCard item={item} loading={props.loading}/>
                    </List.Item>
                )}
            />
        </div>
    )
}

const Dashboard = () => {
    const {status, data, error, isFetching} = useQuery({
        queryKey: [`activities`],
    })
    if (error) return <div>Error: {error.message}</div>;

    return (
        <div>
            <Row>
                <Col span={5}/>
                <Col span={14}>
                    {/*<Divider type={"horizontal"} orientation={"center"}>*/}
                    {/*    Monthly Progress*/}
                    {/*</Divider>*/}
                    {/*<MonthlyProgressCard/>*/}
                    <Divider type={"horizontal"} orientation={"center"}>
                        Activity Feed
                    </Divider>
                    <ActivityList data={data} loading={isFetching}/>
                </Col>
                <Col span={5}/>
            </Row>
        </div>
    )
}

export default Dashboard

