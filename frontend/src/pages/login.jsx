import { Button, Form, Input} from 'antd';
import { useAuth } from "../hooks/useAuth";
import axios from 'axios';

const LoginPage = () => {
    const { login } = useAuth();
    const [form] = Form.useForm();

    const onFinish = async (values) => {
        try {
            const response = await axios.post('/auth/login', { email: values.email, password: values.password });
            login(response.data)
        } catch (error) {
            if (error.status === 401) {
                form.setFields([
                    {
                        name: "email",
                        errors: [""],
                    },
                    {
                        name: "password",
                        errors: ["Invalid credentials"],
                    },
                ]);
            } else {
                console.log(error)

            }
        }
    };
    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };



    return (
        <div style={

            {
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                height: "100vh" /* This will center the form vertically */
            }
        }>


            <Form
                form={form}
                name="basic"
                labelCol={{
                    span: 8,
                }}
                wrapperCol={{
                    span: 16,
                }}
                style={{
                    maxWidth: 600,
                }}
                initialValues={{
                    remember: true,
                }}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                autoComplete="off"
            >
                <Form.Item
                    label="E-mail"
                    name="email"
                    rules={[
                        {
                            required: true,
                            message: 'Please input your E-Mail!',
                        },
                    ]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="Password"
                    name="password"
                    rules={[
                        {
                            required: true,
                            message: 'Please input your password!',
                        },
                    ]}
                >
                    <Input.Password />
                </Form.Item>

                <Form.Item label={null}>
                    <Button type="primary" htmlType="submit">
                        Submit
                    </Button>
                </Form.Item>
            </Form>
        </div>
    )

};

export default LoginPage;
