/* eslint-disable react/prop-types */
import {Area, AreaChart, CartesianGrid, ResponsiveContainer, Tooltip, XAxis, YAxis,} from 'recharts';
import {Card, Col, Row, Statistic, Table} from 'antd';
import {
    convertMetersToKilometers,
    convertSecondsToHMS,
    convertTimestampToHMS,
    dateFromFitDateTime, displayLocation, roundFirstDecimal
} from "../components/utils";
import "leaflet/dist/leaflet.css";
import {MapContainer, Polyline, TileLayer} from 'react-leaflet'
import {Link, useParams} from "react-router-dom";
import {useQuery} from "@tanstack/react-query";

const FlexCol = props => {

    return (
        <Col
            xs={{
                flex: '50%',
            }}
            sm={{
                flex: '30%',
            }}

            lg={{
                flex: '16%',
            }}
        >
            {props.children}
        </Col>
    )
}
const ActivityHead = (props) => {
    let data = props.data;
    return (
        <div>

            <Row gutter={16} justify={"space-evenly"} align={"middle"}>
                <FlexCol>
                    <Statistic title="Date" value={dateFromFitDateTime(data.start_time)} />
                </FlexCol>
                <FlexCol>
                    <Statistic title="Location" value={displayLocation(data.location)} />
                </FlexCol>
                <FlexCol>
                    <Statistic title="Duration" value={convertSecondsToHMS(data.total_elapsed_time)} />
                </FlexCol>
                <FlexCol>
                    <Statistic title="Distance" value={convertMetersToKilometers(data.total_distance)} />
                </FlexCol>
                <FlexCol>
                    <Statistic title="Heart-Rate" value={data.avg_heart_rate} precision={0} />
                </FlexCol>
                <FlexCol>
                    <Statistic title="Pace" value={data.avg_speed} precision={2} />
                </FlexCol>
            </Row>

        </div>
    )

}

const ActivityRecords = props => {
    let data = props.data;
    return (
        <div>
            <RecordChart title={"Heart-Rate"} data={data} dataKey={"heart_rate"} />
            <RecordChart title={"Speed"} data={data} dataKey={"enhanced_speed"} yReversed />
            <RecordChart title={"Cadence"} data={data} dataKey={"cadence"} />
        </div>
    )
}

const RecordChart = props => {
    const yDomain = {
        heart_rate: ['dataMin - 10', 'dataMax + 10'],
        enhanced_speed: [0, 6],
        cadence: ['dataMin - 5', 'dataMax + 5'],
    }

    return (

        <Card
            title={props.title}
            bordered={false}
            style={{
                width: "100%",
            }}
        >
            <ResponsiveContainer width="100%" height={200}>
                <AreaChart
                    width={500}
                    height={400}
                    data={props.data}
                    syncId="anyId"
                    margin={{
                        top: 10,
                        right: 30,
                        left: 0,
                        bottom: 0,
                    }}
                >
                    <defs>
                        <linearGradient id={"colorUv"} x1="0" y1="0" x2="0" y2="1">
                            <stop offset="5%%" stopColor={"#002766"} stopOpacity={0.95}/>
                            <stop offset="95%" stopColor={"#096dd9"} stopOpacity={0.6}/>
                        </linearGradient>
                    </defs>
                    <CartesianGrid strokeDasharray="3 3"/>
                    <XAxis dataKey="timestamp"
                           tickFormatter={convertTimestampToHMS}
                    />

                    <Tooltip labelFormatter={convertTimestampToHMS}/>
                    <YAxis reversed={props.yReversed}
                           domain={yDomain[props.dataKey]}/>
                    <Area type="monotone"
                          dataKey={props.dataKey}
                          stroke={"#001529"}
                          fill="url(#colorUv)"
                    />
                </AreaChart>
            </ResponsiveContainer>
        </Card>
    )
}

const ActivityMap = (props) => {
    let tracks = props.data
    tracks = tracks
        .filter(obj => obj.position_long !== 0)
        .map(obj => [obj.position_lat, obj.position_long]);

    let avgLat = tracks.reduce((sum, val) => sum + val[0], 0) / tracks.length;
    let avgLong = tracks.reduce((sum, val) => sum + val[1], 0) / tracks.length;
    let startPosition = [avgLat, avgLong];


    if (tracks.length === 0) return <div>No Map data available for Activity</div>

    return (
        <MapContainer center={startPosition} zoom={13} scrollWheelZoom={false}
                      style={{ height: 300}}>
            <TileLayer
                attribution=''
                url="http://sgx.geodatenzentrum.de/wmts_topplus_open/tile/1.0.0/web/default/WEBMERCATOR/{z}/{y}/{x}.png"
            />
            <Polyline pathOptions={{color: "#5e19b5"}} positions={tracks}/>
        </MapContainer>
    )

}

const MatchedActivities = (props) => {
    const columns = [
        {
            title: 'Date',
            dataIndex: 'start_time',
            key: 'start_time',
            render: (data) => <div>{dateFromFitDateTime(data)}</div>,
        },
        {
            title: 'Location',
            dataIndex: 'location',
            key: 'location',
            render: (data) => <div>{displayLocation(data)}</div>,
        },
        {
            title: 'Distane',
            dataIndex: 'total_distance',
            key: 'total_distance',
            responsive: ['md'],
            render: (data) => <div>{convertMetersToKilometers(data)}</div>,
        },
        {
            title: 'Heart-Rate',
            dataIndex: 'avg_heart_rate',
            key: 'avg_heart_rate',
            responsive: ['md'],
            render: (data) => <div>{data} bpm</div>,
        },
        {
            title: 'Pace',
            dataIndex: 'avg_speed',
            key: 'avg_speed',
            responsive: ['md'],
            render: (data) => <div>{roundFirstDecimal(data)}</div>,
        },
        {
            title: '',
            dataIndex: 'ID',
            key: 'id',
            render: (id) => <Link to={"/session/" + id}>View</Link>,
        },
    ];

    return (
        <div>
            <Card
                title={"Matched Activities"}
                bordered={false}
                style={{
                    width: "100%",
                }}
            >
            <Table columns={columns} dataSource={props.data} rowKey={"ID"}/>
            </Card>
        </div>
    )
}

const ActivityDetail = () => {
    let { sessionId } = useParams();
    const {  data, error, isFetching } = useQuery({
        queryKey: [`activities/${sessionId}`],
    })
    if (error) return <div>Error: {error.message}</div>;

    if (isFetching) return <div>Loading</div>
    return (
        <div>
            <ActivityHead data={data.session} />
            <ActivityRecords data={data.records} />
            <MatchedActivities data={data.matched_activities}/>
            <ActivityMap data={data.records}/>
        </div>
    )
}



export default ActivityDetail;
