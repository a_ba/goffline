/* eslint-disable react/prop-types */
import {Button, Divider, Form, Input, notification, Space} from 'antd';
import {useMutation, useQuery} from "@tanstack/react-query";
import axios from "axios";
import {useState} from "react";

const NextCloudSettings = () => {

    const {status, data, error, isFetching} = useQuery({
        queryKey: ['nextcloudConfig'],
        queryFn: async () => {
            try {
                const response = await axios.get("settings/nextcloud");
                return response.data;
            } catch (error) {
                if (error.response.status === 404) {
                    setInitialConfig(true)
                    return {}
                } else {
                    throw new Error('Unable to load settings');
                }
            }
        },
    })
    const [form] = Form.useForm();
    const [initialConfig, setInitialConfig] = useState(false);
    const onFinish = async (values) => {
        mutation.mutate()
    }

    const mutation = useMutation({
        mutationFn: async () => {
            if (initialConfig === true) {
                return await axios.post('/settings/nextcloud', form.getFieldsValue())
            } else {
                try{
                    let res = await axios.put('/settings/nextcloud', form.getFieldsValue())
                    notification.success({
                        message: 'Update Success',
                        description:
                            'NextCloud configuration updated successfully.',
                    });
                    return res.data
                }
                catch(error) {
                    console.log(error)
                    if (error.response.status === 400  && error.response.data.message.includes("Could not initialize NextCloud client")) {
                        form.setFields([
                            {
                                name: "host",
                                errors: [""],
                            },
                            {
                                name: "token",
                                errors: [""],
                            },
                            {
                                name: "password",
                                errors: ["Could not connect to NextCloud instance - please check your settings and try again"],
                            },
                        ]);
                    } else {
                        notification.error({
                            message: 'Error',
                            description:
                                'Could not update configuration - see the log for details',
                        });
                        throw new Error(error);
                    }
                }
            }
        },
    })

    if (isFetching) return <div>Loading...</div>;
    return (
        <Form
            layout={"vertical"}
            initialValues={data}
            form={form}
            name="basic"
            style={{
                maxWidth: 600,
            }}
            onFinish={onFinish}
            autoComplete="off"
        >
            <Form.Item
                label="id"
                name="ID"
                hidden
            >
                <Input/>
            </Form.Item>
            <Form.Item
                label="email"
                name="email"
                hidden
            >
                <Input/>
            </Form.Item>

            <Form.Item
                label="Hostname"
                name="host"
                rules={[{required: true,},]}
            >
                <Input/>
            </Form.Item>
            <Form.Item
                label="Token"
                name="token"
                rules={[{required: true,},]}
            >
                <Input/>
            </Form.Item>

            <Form.Item
                label="Password"
                name="password"
                rules={[{required: true,},]}
            >
                <Input.Password/>
            </Form.Item>

            <Form.Item label={null}>
                <Space>
                    <Button disabled={mutation.isPending} loading={mutation.isPending} type="primary" htmlType="submit">
                        {initialConfig ? "Save" : "Update"}
                    </Button>
                </Space>
            </Form.Item>
        </Form>
    )
}
const SettingsPage = () => {
    return (
        <div>
            <Divider orientation="left">NextCloud</Divider>
            <NextCloudSettings/>
        </div>
    )
}

export default SettingsPage;
