import { createContext, useContext,useEffect, useMemo } from "react";
import { useNavigate } from "react-router-dom";
import { useLocalStorage } from "./useLocalStorage";
import axios from "axios";
const AuthContext = createContext();

export const AuthProvider = ({ children }) => {
  // https://blog.logrocket.com/authentication-react-router-v6/

  const [token, setToken] = useLocalStorage("token", null);
  const navigate = useNavigate();

  // call this function when you want to authenticate the user
  const login = async (data) => {
    setToken(data);
    navigate("/");
  };

  // call this function to sign out logged in user
  const logout = () => {
    setToken(null);
    navigate("/login", { replace: true });
  };

  // Setup axios interceptor
  useEffect(() => {
    const interceptor = axios.interceptors.response.use(
        (response) => response,
        (error) => {
          if (error.response && error.response.status === 401) {
            logout(); // Call logout when 401 error is received
          }
          return Promise.reject(error);
        }
    );

    // Cleanup interceptor on unmount
    return () => {
      axios.interceptors.response.eject(interceptor);
    };
  }, [logout]); // Add logout to dependency array to ensure latest logout function is used


  const value = useMemo(
    () => ({
      token,
      login,
      logout,
    }),
    [token]
  );
  return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
};

export const useAuth = () => {
  const context = useContext(AuthContext);
  if (context === null) {
    throw new Error('useAuth must be used within an AuthProvider');
  }
  return context;
};
