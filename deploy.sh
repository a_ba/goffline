#!/bin/bash

# Exit on any error
set -e

# Colors for output
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[1;33m'
NC='\033[0m' # No Color

# Log function
log() {
    echo -e "${GREEN}[$(date +'%Y-%m-%d %H:%M:%S')] $1${NC}"
}

error() {
    echo -e "${RED}[$(date +'%Y-%m-%d %H:%M:%S')] ERROR: $1${NC}" >&2
}

warn() {
    echo -e "${YELLOW}[$(date +'%Y-%m-%d %H:%M:%S')] WARNING: $1${NC}"
}

# Check if .env file exists
check_env() {
    if [ ! -f .env ]; then
        error "No .env file found!"
        exit 1
    fi
    log "Environment file found"
}

# Pull latest changes
pull_changes() {
    log "Pulling latest changes from repository..."
    git pull origin master || {
        error "Failed to pull latest changes"
        exit 1
    }
}

# Build and deploy services
deploy() {
    log "Starting deployment process..."

    # Stop any running services
    log "Stopping existing services..."
    docker compose down || warn "No existing services to stop"

    # Build services with production profile
    log "Building services..."
    docker compose --profile prod build \
        --build-arg BUILDKIT_INLINE_CACHE=1 || {
        error "Failed to build services"
        exit 1
    }

    # Start services in detached mode
    log "Starting services..."
    docker compose --profile prod up -d || {
        error "Failed to start services"
        exit 1
    }

    # Check if services are running
    log "Checking service health..."
    sleep 10  # Wait for services to initialize

    # Check if postgres is healthy
    if ! docker compose ps postgres | grep -q "healthy"; then
        error "Postgres is not healthy"
        exit 1
    fi

    # Check if app is running
    if ! docker compose ps app | grep -q "Up"; then
        error "App service is not running"
        exit 1
    fi

    # Check if frontend is running
    if ! docker compose ps frontend | grep -q "Up"; then
        error "Frontend service is not running"
        exit 1
    fi

    log "Deployment completed successfully!"
}

# Cleanup function
cleanup() {
    log "Cleaning up old images..."
    docker image prune -f
}

# Main execution
main() {
    log "Starting deployment script..."

    check_env
    pull_changes
    deploy
    cleanup

    log "All done! 🚀"
}

# Run main function
main
